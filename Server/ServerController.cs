﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Server
{
    public class ServerController : ApiController
    {
        private const string SERVER_STORAGE_PATH = "files";

        [HttpGet]
        public IHttpActionResult ListFiles()
        {
            return Ok(readFiles());
        }

        [HttpGet]
        public IHttpActionResult DownloadFile([FromUri]string fileName = null)
        {
            var availableFiles = readFiles();
            if (!availableFiles.Contains(fileName))
            {
                return NotFound();
            }

            try

            {
                MemoryStream responseStream = new MemoryStream();
                byte[] bytes = File.ReadAllBytes($"{SERVER_STORAGE_PATH}/{fileName}");

                HttpResponseMessage response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;
                response.Content = new StreamContent(new MemoryStream(bytes));
                return ResponseMessage(response);
            }
            catch (IOException exception)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    "Exception happens while attempting to process the file"));
            }
        }

        private List<string> readFiles()
        {
            var files = new List<string>();
            foreach (string file in Directory.EnumerateFiles(SERVER_STORAGE_PATH))
            {
                files.Add(Path.GetFileName(file));
            }
            return files;
        }
    }
}
